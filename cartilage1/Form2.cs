﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace cartilage
{
    public partial class Form2 : Form
    {

        private Form1 mainForm = null;
        public Form2(Form callingForm)
        {
            mainForm = callingForm as Form1;
            InitializeComponent();
        }


        public static bool[] posts = new bool[24];

        public Form2()
        {
         
            for (int i = 0; i < 23; i++) posts[i] = true;

            InitializeComponent();
            
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String p="";
            for (int i = 1; i < 25; i++)
            {
                CheckBox check = (CheckBox)this.Controls["c" + i.ToString()];
                 posts[i-1] = check.Checked;
                if (check.Checked) p = p + "1" + Environment.NewLine; else p = p + "0" + Environment.NewLine;

             

            }

            SaveFileDialog save = new SaveFileDialog();
            save.FileName = "posts.txt";
            save.Filter = "Text File | *.txt";
            if (save.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(save.OpenFile()))
                {
                    sw.Write(p);
                    sw.Dispose();
                    sw.Close();


                }
            }
           

        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 1; i < 25; i++)
            {
                CheckBox check = (CheckBox)this.Controls["c" + i.ToString()];

                Form1.check1 = i-1;
              if (check.Checked)   Form1.check1_val = 1; else Form1.check1_val = 0;

              this.mainForm.update_graph = 1;

            

            }
        }
    }
}
