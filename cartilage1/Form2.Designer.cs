﻿namespace cartilage
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.c6 = new System.Windows.Forms.CheckBox();
            this.c5 = new System.Windows.Forms.CheckBox();
            this.c4 = new System.Windows.Forms.CheckBox();
            this.c1 = new System.Windows.Forms.CheckBox();
            this.c2 = new System.Windows.Forms.CheckBox();
            this.c3 = new System.Windows.Forms.CheckBox();
            this.c12 = new System.Windows.Forms.CheckBox();
            this.c11 = new System.Windows.Forms.CheckBox();
            this.c10 = new System.Windows.Forms.CheckBox();
            this.c9 = new System.Windows.Forms.CheckBox();
            this.c8 = new System.Windows.Forms.CheckBox();
            this.c7 = new System.Windows.Forms.CheckBox();
            this.c24 = new System.Windows.Forms.CheckBox();
            this.c23 = new System.Windows.Forms.CheckBox();
            this.c22 = new System.Windows.Forms.CheckBox();
            this.c21 = new System.Windows.Forms.CheckBox();
            this.c20 = new System.Windows.Forms.CheckBox();
            this.c19 = new System.Windows.Forms.CheckBox();
            this.c13 = new System.Windows.Forms.CheckBox();
            this.c14 = new System.Windows.Forms.CheckBox();
            this.c15 = new System.Windows.Forms.CheckBox();
            this.c16 = new System.Windows.Forms.CheckBox();
            this.c17 = new System.Windows.Forms.CheckBox();
            this.c18 = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // c6
            // 
            this.c6.AutoSize = true;
            this.c6.Location = new System.Drawing.Point(35, 25);
            this.c6.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c6.Name = "c6";
            this.c6.Size = new System.Drawing.Size(32, 17);
            this.c6.TabIndex = 0;
            this.c6.Text = "6";
            this.c6.UseVisualStyleBackColor = true;
            // 
            // c5
            // 
            this.c5.AutoSize = true;
            this.c5.Location = new System.Drawing.Point(78, 25);
            this.c5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c5.Name = "c5";
            this.c5.Size = new System.Drawing.Size(32, 17);
            this.c5.TabIndex = 1;
            this.c5.Text = "5";
            this.c5.UseVisualStyleBackColor = true;
            // 
            // c4
            // 
            this.c4.AutoSize = true;
            this.c4.Location = new System.Drawing.Point(123, 25);
            this.c4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c4.Name = "c4";
            this.c4.Size = new System.Drawing.Size(32, 17);
            this.c4.TabIndex = 2;
            this.c4.Text = "4";
            this.c4.UseVisualStyleBackColor = true;
            // 
            // c1
            // 
            this.c1.AutoSize = true;
            this.c1.Location = new System.Drawing.Point(270, 25);
            this.c1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c1.Name = "c1";
            this.c1.Size = new System.Drawing.Size(32, 17);
            this.c1.TabIndex = 5;
            this.c1.Text = "1";
            this.c1.UseVisualStyleBackColor = true;
            // 
            // c2
            // 
            this.c2.AutoSize = true;
            this.c2.Location = new System.Drawing.Point(218, 25);
            this.c2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c2.Name = "c2";
            this.c2.Size = new System.Drawing.Size(32, 17);
            this.c2.TabIndex = 4;
            this.c2.Text = "2";
            this.c2.UseVisualStyleBackColor = true;
            // 
            // c3
            // 
            this.c3.AutoSize = true;
            this.c3.Location = new System.Drawing.Point(170, 25);
            this.c3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c3.Name = "c3";
            this.c3.Size = new System.Drawing.Size(32, 17);
            this.c3.TabIndex = 3;
            this.c3.Text = "3";
            this.c3.UseVisualStyleBackColor = true;
            // 
            // c12
            // 
            this.c12.AutoSize = true;
            this.c12.Location = new System.Drawing.Point(270, 67);
            this.c12.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c12.Name = "c12";
            this.c12.Size = new System.Drawing.Size(38, 17);
            this.c12.TabIndex = 11;
            this.c12.Text = "12";
            this.c12.UseVisualStyleBackColor = true;
            // 
            // c11
            // 
            this.c11.AutoSize = true;
            this.c11.Location = new System.Drawing.Point(218, 67);
            this.c11.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c11.Name = "c11";
            this.c11.Size = new System.Drawing.Size(38, 17);
            this.c11.TabIndex = 10;
            this.c11.Text = "11";
            this.c11.UseVisualStyleBackColor = true;
            // 
            // c10
            // 
            this.c10.AutoSize = true;
            this.c10.Location = new System.Drawing.Point(170, 67);
            this.c10.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c10.Name = "c10";
            this.c10.Size = new System.Drawing.Size(38, 17);
            this.c10.TabIndex = 9;
            this.c10.Text = "10";
            this.c10.UseVisualStyleBackColor = true;
            // 
            // c9
            // 
            this.c9.AutoSize = true;
            this.c9.Location = new System.Drawing.Point(123, 67);
            this.c9.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c9.Name = "c9";
            this.c9.Size = new System.Drawing.Size(32, 17);
            this.c9.TabIndex = 8;
            this.c9.Text = "9";
            this.c9.UseVisualStyleBackColor = true;
            // 
            // c8
            // 
            this.c8.AutoSize = true;
            this.c8.Location = new System.Drawing.Point(78, 67);
            this.c8.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c8.Name = "c8";
            this.c8.Size = new System.Drawing.Size(32, 17);
            this.c8.TabIndex = 7;
            this.c8.Text = "8";
            this.c8.UseVisualStyleBackColor = true;
            // 
            // c7
            // 
            this.c7.AutoSize = true;
            this.c7.Location = new System.Drawing.Point(35, 67);
            this.c7.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c7.Name = "c7";
            this.c7.Size = new System.Drawing.Size(32, 17);
            this.c7.TabIndex = 6;
            this.c7.Text = "7";
            this.c7.UseVisualStyleBackColor = true;
            // 
            // c24
            // 
            this.c24.AutoSize = true;
            this.c24.Location = new System.Drawing.Point(270, 150);
            this.c24.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c24.Name = "c24";
            this.c24.Size = new System.Drawing.Size(38, 17);
            this.c24.TabIndex = 23;
            this.c24.Text = "24";
            this.c24.UseVisualStyleBackColor = true;
            // 
            // c23
            // 
            this.c23.AutoSize = true;
            this.c23.Location = new System.Drawing.Point(218, 150);
            this.c23.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c23.Name = "c23";
            this.c23.Size = new System.Drawing.Size(38, 17);
            this.c23.TabIndex = 22;
            this.c23.Text = "23";
            this.c23.UseVisualStyleBackColor = true;
            // 
            // c22
            // 
            this.c22.AutoSize = true;
            this.c22.Location = new System.Drawing.Point(170, 150);
            this.c22.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c22.Name = "c22";
            this.c22.Size = new System.Drawing.Size(38, 17);
            this.c22.TabIndex = 21;
            this.c22.Text = "22";
            this.c22.UseVisualStyleBackColor = true;
            // 
            // c21
            // 
            this.c21.AutoSize = true;
            this.c21.Location = new System.Drawing.Point(123, 150);
            this.c21.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c21.Name = "c21";
            this.c21.Size = new System.Drawing.Size(38, 17);
            this.c21.TabIndex = 20;
            this.c21.Text = "21";
            this.c21.UseVisualStyleBackColor = true;
            // 
            // c20
            // 
            this.c20.AutoSize = true;
            this.c20.Location = new System.Drawing.Point(78, 150);
            this.c20.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c20.Name = "c20";
            this.c20.Size = new System.Drawing.Size(38, 17);
            this.c20.TabIndex = 19;
            this.c20.Text = "20";
            this.c20.UseVisualStyleBackColor = true;
            // 
            // c19
            // 
            this.c19.AutoSize = true;
            this.c19.Location = new System.Drawing.Point(35, 150);
            this.c19.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c19.Name = "c19";
            this.c19.Size = new System.Drawing.Size(38, 17);
            this.c19.TabIndex = 18;
            this.c19.Text = "19";
            this.c19.UseVisualStyleBackColor = true;
            // 
            // c13
            // 
            this.c13.AutoSize = true;
            this.c13.Location = new System.Drawing.Point(270, 108);
            this.c13.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c13.Name = "c13";
            this.c13.Size = new System.Drawing.Size(38, 17);
            this.c13.TabIndex = 17;
            this.c13.Text = "13";
            this.c13.UseVisualStyleBackColor = true;
            // 
            // c14
            // 
            this.c14.AutoSize = true;
            this.c14.Location = new System.Drawing.Point(218, 108);
            this.c14.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c14.Name = "c14";
            this.c14.Size = new System.Drawing.Size(38, 17);
            this.c14.TabIndex = 16;
            this.c14.Text = "14";
            this.c14.UseVisualStyleBackColor = true;
            // 
            // c15
            // 
            this.c15.AutoSize = true;
            this.c15.Location = new System.Drawing.Point(170, 108);
            this.c15.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c15.Name = "c15";
            this.c15.Size = new System.Drawing.Size(38, 17);
            this.c15.TabIndex = 15;
            this.c15.Text = "15";
            this.c15.UseVisualStyleBackColor = true;
            // 
            // c16
            // 
            this.c16.AutoSize = true;
            this.c16.Location = new System.Drawing.Point(123, 108);
            this.c16.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c16.Name = "c16";
            this.c16.Size = new System.Drawing.Size(38, 17);
            this.c16.TabIndex = 14;
            this.c16.Text = "16";
            this.c16.UseVisualStyleBackColor = true;
            // 
            // c17
            // 
            this.c17.AutoSize = true;
            this.c17.Location = new System.Drawing.Point(78, 108);
            this.c17.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c17.Name = "c17";
            this.c17.Size = new System.Drawing.Size(38, 17);
            this.c17.TabIndex = 13;
            this.c17.Text = "17";
            this.c17.UseVisualStyleBackColor = true;
            // 
            // c18
            // 
            this.c18.AutoSize = true;
            this.c18.Location = new System.Drawing.Point(35, 108);
            this.c18.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.c18.Name = "c18";
            this.c18.Size = new System.Drawing.Size(38, 17);
            this.c18.TabIndex = 12;
            this.c18.Text = "18";
            this.c18.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(170, 186);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 22);
            this.button1.TabIndex = 24;
            this.button1.Text = "Save posts file..";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(40, 186);
            this.button2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(126, 22);
            this.button2.TabIndex = 25;
            this.button2.Text = "Choose posts on graph";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 217);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.c24);
            this.Controls.Add(this.c23);
            this.Controls.Add(this.c22);
            this.Controls.Add(this.c21);
            this.Controls.Add(this.c20);
            this.Controls.Add(this.c19);
            this.Controls.Add(this.c13);
            this.Controls.Add(this.c14);
            this.Controls.Add(this.c15);
            this.Controls.Add(this.c16);
            this.Controls.Add(this.c17);
            this.Controls.Add(this.c18);
            this.Controls.Add(this.c12);
            this.Controls.Add(this.c11);
            this.Controls.Add(this.c10);
            this.Controls.Add(this.c9);
            this.Controls.Add(this.c8);
            this.Controls.Add(this.c7);
            this.Controls.Add(this.c1);
            this.Controls.Add(this.c2);
            this.Controls.Add(this.c3);
            this.Controls.Add(this.c4);
            this.Controls.Add(this.c5);
            this.Controls.Add(this.c6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form2";
            this.Text = "Choose which posts to scan";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox c6;
        private System.Windows.Forms.CheckBox c5;
        private System.Windows.Forms.CheckBox c4;
        private System.Windows.Forms.CheckBox c1;
        private System.Windows.Forms.CheckBox c2;
        private System.Windows.Forms.CheckBox c3;
        private System.Windows.Forms.CheckBox c12;
        private System.Windows.Forms.CheckBox c11;
        private System.Windows.Forms.CheckBox c10;
        private System.Windows.Forms.CheckBox c9;
        private System.Windows.Forms.CheckBox c8;
        private System.Windows.Forms.CheckBox c7;
        private System.Windows.Forms.CheckBox c24;
        private System.Windows.Forms.CheckBox c23;
        private System.Windows.Forms.CheckBox c22;
        private System.Windows.Forms.CheckBox c21;
        private System.Windows.Forms.CheckBox c20;
        private System.Windows.Forms.CheckBox c19;
        private System.Windows.Forms.CheckBox c13;
        private System.Windows.Forms.CheckBox c14;
        private System.Windows.Forms.CheckBox c15;
        private System.Windows.Forms.CheckBox c16;
        private System.Windows.Forms.CheckBox c17;
        private System.Windows.Forms.CheckBox c18;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}