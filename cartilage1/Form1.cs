﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace cartilage
{
    public partial class Form1 : Form
    {
        Form2 frm;
        string indata;
        int start_line = 1;

        public static int check1;
        public static int check1_val;
       

        void getAvailablePorts()
        {
            comboBox1.Items.Clear();
            string[] ports = SerialPort.GetPortNames();
            comboBox1.Items.AddRange(ports);

            for (int i=0; i<ports.Length-1; i++)
            {
                if (ports[i] == Properties.Settings.Default.lastport) comboBox1.Text = Properties.Settings.Default.lastport;
            }



        }

        public Form1()
        {
            InitializeComponent();
            getAvailablePorts();
        }

        private double f(int i)
        {
            var f1 = 59894 - (8128 * i) + (262 * i * i) - (1.6 * i * i * i);
            return f1;
        }


        private void Form1_Load(object sender, EventArgs e)
        {
          
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frm = new Form2(this);
            frm.Show();

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (arduinoPort.IsOpen) arduinoPort.Close();
            if (comboBox1.Text == "")
            {
                textBox1.AppendText("Please select arduino Port" + "\n");
            }
            else
            {
                try
                {
                    arduinoPort.PortName = comboBox1.Text;
                    arduinoPort.BaudRate = 115200;
                    arduinoPort.Open();
                    if (arduinoPort.IsOpen)
                    {
                        arduinoPort.DataReceived += new SerialDataReceivedEventHandler(DataReceviedHandler);
                        Properties.Settings.Default.lastport = comboBox1.Text;
                        Properties.Settings.Default.Save();
                        textBox1.AppendText("Connection succesfully initialised" + "\n");
                        this.Text = "Cartilage beta testing - active " + comboBox1.Text;
                    }
                }
                catch (Exception err)
                {
                    MessageBox.Show(err.Message);
                }
              
              

            }
        }


        void DataReceviedHandler(object sender, SerialDataReceivedEventArgs e)
        {

            SerialPort sp = (SerialPort)sender;
            indata = sp.ReadExisting();



            this.BeginInvoke(new EventHandler(display_event));


        }
        private void display_event(object sender, EventArgs e)
        {

            if (checkBox1.Checked) textBox1.AppendText(indata); else
            {
                textBox1.Text = textBox1.Text + indata;
            }



        }




       
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            arduinoPort.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.FileName = "Cartilage_Data.txt";
            save.Filter = "Text File | *.txt";
            if (save.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(save.OpenFile()))
                {
                    sw.Write(textBox1.Text);
                    sw.Dispose();
                    sw.Close();
                   

                }
            }


        }
        void refresh_chart()
        {
            string chart_data = "";
            var lines = textBox1.Lines.Count();

            Debug.WriteLine("Lines of txt == " + lines);
            if (lines - start_line > 50)
            {
                for (int i = start_line - 1; i < lines; i++)
                {
                    chart_data = chart_data + textBox1.Lines[i] + Environment.NewLine;
                }

                Match match = Regex.Match(chart_data, @".+?(?= \d+:)");
                Match hour_match = Regex.Match(chart_data, @"(?<=\d\d\d\d  )(.*)(?=:\d+:\d+)");
                Match time_match = Regex.Match(chart_data, @"(?<=\d+:)(.*)(?=   #Post)");
                Match post_match = Regex.Match(chart_data, @"(?<=#Post: )(.*)(?= #Posi)");

                int first_time = 1;
                do
                {
                    int sum = 0;
                    int i = 0;
                    string post = post_match.Value;
                    string time24 = (Int32.Parse(hour_match.Value) % 24).ToString() + ":" + time_match.Value;
                    do
                    {
                        sum = sum + Int32.Parse(match.Value);
                        i++;
                        match = match.NextMatch();
                        post_match = post_match.NextMatch();
                        hour_match = hour_match.NextMatch();
                        time_match = time_match.NextMatch();
                    }
                    while (post == post_match.Value);
                    int result = sum / (i + first_time);
                    //    a = a + result + "  ,   ." + time24 + ".  => " + post + Environment.NewLine;
                    Debug.WriteLine("lines == " + (Int32.Parse(post) - 1).ToString());
                    chart1.Series[Int32.Parse(post) - 1].Points.AddXY(DateTime.ParseExact(time24, "H:m:s", CultureInfo.InvariantCulture), result);
                    first_time = 0;


                    /*string time24 = (Int32.Parse(hour_match.Value) % 24).ToString() + ":" + time_match.Value;
                    a = a + match.Value + "  ,   ." + time24 + ".  => " + post_match.Value + Environment.NewLine;

                    series1.Points.AddXY(DateTime.ParseExact(time24, "H:m:s", CultureInfo.InvariantCulture), match.Value);
                    match = match.NextMatch();
                    hour_match = hour_match.NextMatch();
                    time_match = time_match.NextMatch();
                    post_match = post_match.NextMatch();

                    */
                }
                while (match.Success);


                chart1.ChartAreas[0].AxisX.LabelStyle.Format = "HH:mm:ss";
                chart1.Invalidate();
                start_line = lines;
            }
        }

        private void comboBox1_Click(object sender, EventArgs e)
        {
            if (arduinoPort.IsOpen) arduinoPort.Close();
            this.Text = "Cartilage beta testing - inactive";
            getAvailablePorts();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
         if (checkBox2.Checked)   refresh_chart();
        }

          

        private void button3_Click(object sender, EventArgs e)
        {
            chart1.Series[1].Enabled = false;

            Form2 frm = new Form2(this);
            frm.Show();
        }

        public int update_graph
        {
            get { return check1; }
            set
            {
                Debug.WriteLine(update_graph);
                if (check1_val == 0) chart1.Series[update_graph].Enabled = false;
                else chart1.Series[update_graph].Enabled = true;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            refresh_chart();
        }

      
    }
}
